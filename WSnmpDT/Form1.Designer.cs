﻿namespace WSnmpDT
{
    partial class F_Connect
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.TB_AddOfSer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.B_Connect = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_Oid = new System.Windows.Forms.TextBox();
            this.B_Used = new System.Windows.Forms.Button();
            this.RTB_Result = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PB_DiskUsedRate = new System.Windows.Forms.ProgressBar();
            this.TB_DiskUsedRate = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PB_MemoryUsedRate = new System.Windows.Forms.ProgressBar();
            this.TB_MemoryUsedRate = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TB_CpuUsedRate = new System.Windows.Forms.TextBox();
            this.PB_CpuUsedRate = new System.Windows.Forms.ProgressBar();
            this.B_Refresh = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TB_CpuLimit = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TB_MemoryLimit = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.TB_DiskLimit = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // TB_AddOfSer
            // 
            this.TB_AddOfSer.Location = new System.Drawing.Point(12, 43);
            this.TB_AddOfSer.Name = "TB_AddOfSer";
            this.TB_AddOfSer.Size = new System.Drawing.Size(240, 21);
            this.TB_AddOfSer.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "SNMP服务器地址";
            // 
            // B_Connect
            // 
            this.B_Connect.Location = new System.Drawing.Point(177, 139);
            this.B_Connect.Name = "B_Connect";
            this.B_Connect.Size = new System.Drawing.Size(75, 23);
            this.B_Connect.TabIndex = 2;
            this.B_Connect.Text = "连接";
            this.B_Connect.UseVisualStyleBackColor = true;
            this.B_Connect.Click += new System.EventHandler(this.B_Connect_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "使用的OID";
            // 
            // TB_Oid
            // 
            this.TB_Oid.Location = new System.Drawing.Point(13, 102);
            this.TB_Oid.Name = "TB_Oid";
            this.TB_Oid.Size = new System.Drawing.Size(239, 21);
            this.TB_Oid.TabIndex = 5;
            // 
            // B_Used
            // 
            this.B_Used.Location = new System.Drawing.Point(15, 203);
            this.B_Used.Name = "B_Used";
            this.B_Used.Size = new System.Drawing.Size(149, 23);
            this.B_Used.TabIndex = 6;
            this.B_Used.Text = "查看主机资源使用情况";
            this.B_Used.UseVisualStyleBackColor = true;
            this.B_Used.Click += new System.EventHandler(this.B_Used_Click);
            // 
            // RTB_Result
            // 
            this.RTB_Result.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RTB_Result.Location = new System.Drawing.Point(278, 43);
            this.RTB_Result.Name = "RTB_Result";
            this.RTB_Result.ReadOnly = true;
            this.RTB_Result.Size = new System.Drawing.Size(336, 80);
            this.RTB_Result.TabIndex = 7;
            this.RTB_Result.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(276, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "结果";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(276, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "磁盘：";
            // 
            // PB_DiskUsedRate
            // 
            this.PB_DiskUsedRate.Location = new System.Drawing.Point(400, 139);
            this.PB_DiskUsedRate.Name = "PB_DiskUsedRate";
            this.PB_DiskUsedRate.Size = new System.Drawing.Size(214, 22);
            this.PB_DiskUsedRate.TabIndex = 10;
            // 
            // TB_DiskUsedRate
            // 
            this.TB_DiskUsedRate.Location = new System.Drawing.Point(324, 139);
            this.TB_DiskUsedRate.Name = "TB_DiskUsedRate";
            this.TB_DiskUsedRate.ReadOnly = true;
            this.TB_DiskUsedRate.Size = new System.Drawing.Size(70, 21);
            this.TB_DiskUsedRate.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(276, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "内存:";
            // 
            // PB_MemoryUsedRate
            // 
            this.PB_MemoryUsedRate.Location = new System.Drawing.Point(400, 171);
            this.PB_MemoryUsedRate.Name = "PB_MemoryUsedRate";
            this.PB_MemoryUsedRate.Size = new System.Drawing.Size(214, 22);
            this.PB_MemoryUsedRate.TabIndex = 13;
            // 
            // TB_MemoryUsedRate
            // 
            this.TB_MemoryUsedRate.Location = new System.Drawing.Point(324, 172);
            this.TB_MemoryUsedRate.Name = "TB_MemoryUsedRate";
            this.TB_MemoryUsedRate.ReadOnly = true;
            this.TB_MemoryUsedRate.Size = new System.Drawing.Size(70, 21);
            this.TB_MemoryUsedRate.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(278, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "CPU:";
            // 
            // TB_CpuUsedRate
            // 
            this.TB_CpuUsedRate.Location = new System.Drawing.Point(324, 200);
            this.TB_CpuUsedRate.Name = "TB_CpuUsedRate";
            this.TB_CpuUsedRate.ReadOnly = true;
            this.TB_CpuUsedRate.Size = new System.Drawing.Size(70, 21);
            this.TB_CpuUsedRate.TabIndex = 16;
            // 
            // PB_CpuUsedRate
            // 
            this.PB_CpuUsedRate.Location = new System.Drawing.Point(400, 199);
            this.PB_CpuUsedRate.Name = "PB_CpuUsedRate";
            this.PB_CpuUsedRate.Size = new System.Drawing.Size(214, 22);
            this.PB_CpuUsedRate.TabIndex = 17;
            // 
            // B_Refresh
            // 
            this.B_Refresh.Location = new System.Drawing.Point(177, 203);
            this.B_Refresh.Name = "B_Refresh";
            this.B_Refresh.Size = new System.Drawing.Size(75, 23);
            this.B_Refresh.TabIndex = 18;
            this.B_Refresh.Text = "刷新";
            this.B_Refresh.UseVisualStyleBackColor = true;
            this.B_Refresh.Click += new System.EventHandler(this.B_Refresh_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 241);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 19;
            this.label7.Text = "阈值：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(63, 241);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 12);
            this.label8.TabIndex = 20;
            this.label8.Text = "CPU：";
            // 
            // TB_CpuLimit
            // 
            this.TB_CpuLimit.Location = new System.Drawing.Point(104, 238);
            this.TB_CpuLimit.Name = "TB_CpuLimit";
            this.TB_CpuLimit.Size = new System.Drawing.Size(60, 21);
            this.TB_CpuLimit.TabIndex = 21;
            this.TB_CpuLimit.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(187, 241);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 12);
            this.label9.TabIndex = 22;
            this.label9.Text = "内存:";
            // 
            // TB_MemoryLimit
            // 
            this.TB_MemoryLimit.Location = new System.Drawing.Point(237, 238);
            this.TB_MemoryLimit.Name = "TB_MemoryLimit";
            this.TB_MemoryLimit.Size = new System.Drawing.Size(68, 21);
            this.TB_MemoryLimit.TabIndex = 23;
            this.TB_MemoryLimit.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(322, 241);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 12);
            this.label10.TabIndex = 24;
            this.label10.Text = "磁盘:";
            // 
            // TB_DiskLimit
            // 
            this.TB_DiskLimit.Location = new System.Drawing.Point(372, 238);
            this.TB_DiskLimit.Name = "TB_DiskLimit";
            this.TB_DiskLimit.Size = new System.Drawing.Size(76, 21);
            this.TB_DiskLimit.TabIndex = 25;
            this.TB_DiskLimit.Text = "0";
            // 
            // F_Connect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(635, 265);
            this.Controls.Add(this.TB_DiskLimit);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.TB_MemoryLimit);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TB_CpuLimit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.B_Refresh);
            this.Controls.Add(this.PB_CpuUsedRate);
            this.Controls.Add(this.TB_CpuUsedRate);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TB_MemoryUsedRate);
            this.Controls.Add(this.PB_MemoryUsedRate);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TB_DiskUsedRate);
            this.Controls.Add(this.PB_DiskUsedRate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RTB_Result);
            this.Controls.Add(this.B_Used);
            this.Controls.Add(this.TB_Oid);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.B_Connect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB_AddOfSer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "F_Connect";
            this.Text = "WSnmpDT";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TB_AddOfSer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button B_Connect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_Oid;
        private System.Windows.Forms.Button B_Used;
        private System.Windows.Forms.RichTextBox RTB_Result;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar PB_DiskUsedRate;
        private System.Windows.Forms.TextBox TB_DiskUsedRate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ProgressBar PB_MemoryUsedRate;
        private System.Windows.Forms.TextBox TB_MemoryUsedRate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TB_CpuUsedRate;
        private System.Windows.Forms.ProgressBar PB_CpuUsedRate;
        private System.Windows.Forms.Button B_Refresh;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TB_CpuLimit;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TB_MemoryLimit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TB_DiskLimit;
    }
}

