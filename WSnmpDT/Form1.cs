﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SnmpSharpNet;

namespace WSnmpDT
{
    public partial class F_Connect : Form
    {
        //磁盘信息的OID
        public static string[] OID_DiskInformation = { "1.3.6.1.2.1.25.2.3.1.3.1", "1.3.6.1.2.1.25.2.3.1.3.2", "1.3.6.1.2.1.25.2.3.1.3.3", "1.3.6.1.2.1.25.2.3.1.3.4", "1.3.6.1.2.1.25.2.3.1.3.10" };
        //磁盘每个块的大小
        public static string[] OID_StorageAllocationUnits = { "1.3.6.1.2.1.25.2.3.1.4.1", "1.3.6.1.2.1.25.2.3.1.4.2", "1.3.6.1.2.1.25.2.3.1.4.3", "1.3.6.1.2.1.25.2.3.1.4.4", "1.3.6.1.2.1.25.2.3.1.4.10" };
        //磁盘中总共的块数
        public static string[] OID_StorageSize = { "1.3.6.1.2.1.25.2.3.1.5.1", "1.3.6.1.2.1.25.2.3.1.5.2", "1.3.6.1.2.1.25.2.3.1.5.3", "1.3.6.1.2.1.25.2.3.1.5.4", "1.3.6.1.2.1.25.2.3.1.5.10" };
        //磁盘中使用了的块数
        public static string[] OID_StorageUsed = { "1.3.6.1.2.1.25.2.3.1.6.1", "1.3.6.1.2.1.25.2.3.1.6.2", "1.3.6.1.2.1.25.2.3.1.6.3", "1.3.6.1.2.1.25.2.3.1.6.4", "1.3.6.1.2.1.25.2.3.1.6.10" };
        public F_Connect()
        {
            InitializeComponent();
        }

        private void B_Connect_Click(object sender, EventArgs e)
        {
            string ServerAddr = TB_AddOfSer.Text.Trim();
            string AskOid = TB_Oid.Text.Trim();
            OctetString community = new OctetString("public");

            AgentParameters param = new AgentParameters(community);
            param.Version = SnmpVersion.Ver1;
            IpAddress agent = new IpAddress(ServerAddr);

            // Construct target
            UdpTarget target = new UdpTarget((IPAddress)agent, 161, 2000, 1);

            // Pdu class used for all requests
            Pdu pdu = new Pdu(PduType.Get);
            pdu.VbList.Add(AskOid);
            SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);

            if (result != null)
            {
                
                if (result.Pdu.ErrorStatus != 0)
                {
                    // agent reported an error with the request
                    this.RTB_Result.Text = string.Format("Error in SNMP reply. Error {0} index {1} \r\n",
                        result.Pdu.ErrorStatus,
                        result.Pdu.ErrorIndex);
                }
                else
                {
                    this.RTB_Result.Text = result.Pdu.VbList[0].Value.ToString();

                }
            }
            else
            {
                this.RTB_Result.Text = "No response received from SNMP agent";
            }
            target.Dispose();
        }

        private void B_Used_Click(object sender, EventArgs e)
        {
            GetMessage();

        }

        private void B_Refresh_Click(object sender, EventArgs e)
        {
            GetMessage();
        }
        private void GetMessage()
        {
            int cpulimit = int.Parse(TB_CpuLimit.Text.ToString());
            int disklimit = int.Parse(TB_DiskLimit.Text.ToString());
            int memorylimit = int.Parse(TB_MemoryLimit.Text.ToString());
            string ServerAddr = TB_AddOfSer.Text.Trim();
            
            OctetString community = new OctetString("public");

            AgentParameters param = new AgentParameters(community);
            param.Version = SnmpVersion.Ver1;
            IpAddress agent = new IpAddress(ServerAddr);

            // Construct target
            UdpTarget target = new UdpTarget((IPAddress)agent, 161, 20000, 1);

            // Pdu class used for all requests
            Pdu pdu = new Pdu(PduType.Get);
            for (int i = 0; i < 5; i++)
            {
                pdu.VbList.Add(OID_DiskInformation[i]);
                pdu.VbList.Add(OID_StorageAllocationUnits[i]);
                pdu.VbList.Add(OID_StorageSize[i]);
                pdu.VbList.Add(OID_StorageUsed[i]);
            }

            SnmpV1Packet result = (SnmpV1Packet)target.Request(pdu, param);
            if (result != null)
            {
                //Designed and Program BY HuZhiFeng  
                // the Agent - see SnmpConstants for error definitions
                if (result.Pdu.ErrorStatus != 0)
                {
                    // agent reported an error with the request
                    this.RTB_Result.Text = string.Format("Error in SNMP reply. Error {0} index {1} \r\n",
                        result.Pdu.ErrorStatus,
                        result.Pdu.ErrorIndex);
                }
                else
                {
                    // Reply variables are returned in the same order as they were added
                    //  to the VbList
                    //RTB_Result.Text+="磁盘:\r\n";
                    List<string> DiskName = new List<string>();
                    List<long> SAU = new List<long>();
                    List<long> SS = new List<long>();
                    List<long> SU = new List<long>();
                    for (int i = 0; i < result.Pdu.VbCount; i++)
                    {
                        switch (i % 4)
                        {
                            case 0: DiskName.Add(result.Pdu.VbList[i].Value.ToString()); break;
                            case 1: SAU.Add(long.Parse(result.Pdu.VbList[i].Value.ToString())); break;
                            case 2: SS.Add(long.Parse(result.Pdu.VbList[i].Value.ToString())); break;
                            case 3: SU.Add(long.Parse(result.Pdu.VbList[i].Value.ToString())); break;
                        }
                    }
                    long total_size = 0;
                    long used_size = 0;

                    int max = (result.Pdu.VbCount - 4) / 4;
                    for (int i = 0; i < max; i++)
                    {

                        total_size += SAU[i] * SS[i] / 1024 / 1024 / 1024;
                        used_size += SAU[i] * SU[i] / 1024 / 1024 / 1024;

                    }
                    double diskusedrate = ((double)used_size / (double)total_size) * 100;
                    double memoryusedrate = ((double)SU[max] / (double)SS[max]) * 100;
                    if (disklimit != 0)
                    {
                        if (diskusedrate > disklimit)
                            MessageBox.Show("警告，当前磁盘使用情况超过阈值！");
                    }
                    if (memorylimit != 0)
                    {
                        if (memorylimit < memoryusedrate)
                            MessageBox.Show("警告，当前内存使用情况超过阈值！");
                    }
                    TB_DiskUsedRate.Text = diskusedrate.ToString() + "%";
                    TB_MemoryUsedRate.Text = memoryusedrate.ToString() + "%";

                    PB_DiskUsedRate.Maximum = 100;
                    PB_DiskUsedRate.Minimum = 0;
                    PB_DiskUsedRate.Value = (int)diskusedrate;

                    PB_MemoryUsedRate.Maximum = 100;
                    PB_MemoryUsedRate.Minimum = 0;
                    PB_MemoryUsedRate.Value = (int)memoryusedrate;
                }
            }
            else
            {
                this.RTB_Result.Text = "No response received from SNMP agent.";
            }
            target.Close();
        }
    }

}
